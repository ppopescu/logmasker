# LogMasker - Easy Masking of Sensitive Data in Logs

!["LogMasker Banner"](http://petrepopescu.tech/wp-content/uploads/2022/03/banner.png)

LogMasker is an easy-to-use library that allows your application to mask sensitive information before it is written to the log, or a log Personally Identifiable Information Reduction (PII Reduction) library. It does the masking directly on the log stream and minimizes the risk of sensitive data appearing inside any printed log.

This is done by intercepting the log event and masking it even before it has a chance to be written. Works with Log4j2 as well as Logback.

## What sensitive data can be masked

Currently, LogMasker can mask the following sensitive information:

- [x] Email addresses
- [x] IPv4 addresses
- [x] IBANs
- [x] Card numbers (PANs)
- [x] Passwords (if marked accordingly)

The maskers that are being used are easily configurable and the library allows you to write your own masker and include it in the masking process. By default, all maskers from above are used.

## Website

The project does not have an official website yet, however, you can visit my Programming Website at [https://petrepopescu.tech](https://petrepopescu.tech). You will find tutorials on Java, Play Framework, Log4j and more. 

## Supporting the project

The library is free can be used inside your project or application even for commercial purposes. If you want to help, you can do so by
- Submitting bug reports
- Contributing to the code
- Donating: 

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/ppopescu)

## Download

### Using Maven Repository
The libraries are available in Maven Central and can be included in your project. There are two versions, one for Log4j2 and one for Logback. Use the appropriate version, depending on your logging sollution.

#### Log4j2
```xml
<dependency>
  <groupId>tech.petrepopescu.logging</groupId>
  <artifactId>log4j2</artifactId>
</dependency>
```

```groovy
implementation 'tech.petrepopescu.logging:log4j2:+'
```

#### Logback
```xml
<dependency>
  <groupId>tech.petrepopescu.logging</groupId>
  <artifactId>logback</artifactId>
</dependency>
```

```groovy
implementation 'tech.petrepopescu.logging:logback:+'
```

### Download JAR files

To download the latest libraries, head over to the `Tags` from the menu on the left. There, select the version you want to download and on the right you will see a 'Download' icon. From there you can retrieve the `build` artifact that contains the jar files for both Log4j2 and Logback.

Alternatively, use this link, however, it may not be the latest version:
[Logmasker v1.0.0-RC4](https://gitlab.com/ppopescu/logmasker/-/jobs/artifacts/v1.0.0-RC4/download?job=build)

## Performance

Each masker ads an additional layer of processing, so it is recommended that you only use the maskers that are needed for your business needs, especially if you have high throughput and write a lot of logs. There are performance tests for each masker as well as for integration directly with Log4j. Results on my machine are as follows:

| Masker                          | Number of lines masked | Average time in ms (for all lines) |
|---------------------------------|------------------------|------------------------------------|
| Email masker                    | 100000                 | 48ms                               |
| Password masker                 | 100000                 | 42ms                               |
| IP masker                       | 100000                 | 46ms                               |
| Card number masker              | 100000                 | 44ms                               |
| IBAN masker (all countries)     | 100000                 | 47ms                               |
| Masking Converter (all maskers) | 100000                 | 115ms                              |
| Log4j Integrated                | 100000                 | 200ms                              |

The tests were executed on a set of lines that were randomly selected. Each line includes at max one item that can be masked. 

## License
The library is developed under Apache License v2.0. For more information read the License file.

## Why some code is not elegant

The library was build using speed in mind. As a result, some parts of the maskers are not that elegant. Methods are longer than they should normally be since they were made to use the minimum number of new objects that are being created for each log event. Also, primitives are being used whenever possible.

## Basic Configuration
### Integration with Log4j 2

The library can be easily added and integrated with Log4J 2. Once imported into your project, it will provide a custom message converter (`LogEventPatternConverter`) which when used will mask all incoming data. To do this, replace the `%m` inside your message pattern with `%msk` or `%mask`. 

Example log4j2.xml file:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="tech.petrepopescu.logging">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level - %msk%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

### Integration with Logback

The library can be easily added and integrated with Logback. Once imported into your project, it will provide a custom message converter (`MessageConverter`) which can be used for al lmessages. To do this, include the following line in your configuration file:

```xml
<conversionRule conversionWord="mask" converterClass="LogbackMaskingConverter" />
```

After that, replace the `%message` converter with the new `%mask` one. Example `logback.xml` file:

```xml
<configuration>
    <conversionRule conversionWord="mask" converterClass="LogbackMaskingConverter" />

    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %mask%n</pattern>
        </encoder>
    </appender>

    <root level="debug">
        <appender-ref ref="STDOUT" />
    </root>
</configuration>
```

### Configuration options

The Converter allows you to configure what maskers are to be used. Bellow you will find the available maskers an examples for Log4j2. The same options are available for Logback as well by adapting to the Logback XML format. In Logback, options are separated by `,` and not in separate groups delimited by `{` and `}`. 

Example:

| Log4j2 XML        | Logback XML        |
|-------------------|--------------------|
| `%msk{email}{ip}` | `%mask{email, ip}` |

Maskers and their associated option:

| Masker             | Option to include it | Additional configuration options |
|--------------------|----------------------|----------------------------------|
| Email Masker       | {email}              | -                                |
| Password masker    | {pass}               | -                                |
| IP Masker          | {ip}                 | -                                |
| Card number masker | {card}               | :noStartDigits&#124;noEndDigits  |
| IBAN masker        | {iban}               | :CC&#124;CC&#124;CC              |

#### Configuring which maskers to use

By default, all maskers from above are used, however, this can easily be changed based on your needs. To do this, include the masker's keyword in your Log4J2 XML as an option for the `%msk` pattern. Additionally, there is an `{ALL}` option that can be used to include all the above maskers. This is useful for adding your own masker alongside the pre-defined ones.

Example configuration that only masks emails and IPs:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="tech.petrepopescu.logging">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level - %msk{email}{ip}%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

##### Configuring countries for IBAN masker
Since there are multiple formats for IBAN depending on the country, and masking all may take a long time, it is possible to specify only certain countries to be included. To do this, include the country codes, separated by `|` as an option for the IBAN masker. By default, all 79 countries are being used.

Example configuration that only masks IBANs in Romania and Germany:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="tech.petrepopescu.logging">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level - %msk{iban:RO|DE}%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

#### Including a custom masker

You can include custom maskers with ease. To do this, first write one or more maskers classes that implement the `LogMasker` interface (from `tech.petrepopescu.logging.masker`). Next, specify the package where it is as an option to the masker configuration inside the XML file. Multiple packages can be included by separating them with `|`.

Example configuration file that uses a custom masker
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="tech.petrepopescu.logging">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level - %msk{custom:tech.petrepopescu.example}%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

#### Masking order

The maskers are used in sequential order. The order is dictated by the order of the options inside the XML file. If no configuration options are provided, or if the `ALL` option is used, the order is as follows: email, password, IP, Card Number, IBAN.

## Advanced Configuration

The library accepts more advanced configuration which can be done using the XML file for the appropriate logging library of your choice. It will allow even more configuration options compared to the basic configuration, however, the XML will be more complex

### Log4j Configuration

For Log4j, a new Layout has been created that wraps the `PatternLayout` and provides additional configuration options. Inside the new `MaskingLayout` you can have a `PatternLayout` which can be used as normal, without the need to use the `msk` keyword. Just use the normal `%m`, and the layout will handle the masking.

The `MaskingLayout` accepts the `maskChar` property, where you can specify what character to be used for masking.

Additionally, you can specify which maskers to be used, along with configuration options for each. For more details on configuring each masker, please read the appropriate wiki page.

Below is an example configuration which only masks the card numbers and the passwords:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="tech.petrepopescu.logging">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <MaskingLayout maskChar="%">
                <PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level - %m%n"/>
                <Maskers>
                    <CardNumberMasker startKeep="3" endKeep="5" luhnCheck="true" />
                    <PasswordMasker keywords="password|pass" />
                </Maskers>
            </MaskingLayout>
        </Console>
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

### Logback Configuration

For Logback, a new encoder was used, which acts just like the `PatternLayoutEncoder`. The new `MaskingLayoutEncoder` provided is an extension of the other and it masks the message, without the need to use the `msk` keyword.

Additional configuration parameters can be provided, including the maskers which are to be used. For more information on configuring each masker, read the appropriate entry in the wiki.

Below is a sample XML configuration. 

```xml
<configuration>
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="tech.petrepopescu.logging.logback.MaskingLayoutEncoder">
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
            <maskChar>%</maskChar>
            <masker class="tech.petrepopescu.logging.logback.maskers.LogbackCardNumberMasker">
                <startKeep>3</startKeep>
                <endKeep>5</endKeep>
                <luhnCheck>true</luhnCheck>
            </masker>
            <masker class="tech.petrepopescu.logging.logback.maskers.LogbackIbanMasker">
                <countryCode>RO</countryCode>
            </masker>
            <masker class="tech.petrepopescu.logging.logback.maskers.LogbackPasswordMasker">
                <keyword>password</keyword>
            </masker>
        </encoder>
    </appender>

    <root level="debug">
        <appender-ref ref="STDOUT" />
    </root>
</configuration>
```

### Note
*IMPORTANT* Using the advanced configuration may result in additional overhead when logging messages. This is because Log4j and Logback don't easily allow altering the message and additional objects need to be created.
