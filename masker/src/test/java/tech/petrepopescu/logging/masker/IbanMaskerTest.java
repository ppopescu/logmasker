package tech.petrepopescu.logging.masker;

import tech.petrepopescu.logging.masker.abstracts.AbstractIbanMaskingTest;

public class IbanMaskerTest extends AbstractIbanMaskingTest {

    @Override
    protected LogMasker getLogMasker(String params) {
        LogMasker ibanMasker = new IbanMasker();
        ibanMasker.initialize(params);
        return ibanMasker;
    }
}
