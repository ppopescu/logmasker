package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public abstract class AbstractPasswordMaskerTest extends AbstractMaskingTest {
    protected MaskingConverter masker = initializeConverter();

    @ParameterizedTest
    @ValueSource(strings = {"password:testpass", "password: testpass", "password=testpass", "Password: 123",
            "pass:testpass", "pass: testpass", "pass=testpass","Pass: 123",
            "pwd:testpass", "pwd: testpass", "pwd=testpass", "Pwd: 123",
            "passphrase:testpass", "passphrase: testpass", "passphrase=testpass", "Pwd: 123",
            "<password>testpass</password>", "<password>test pass</password>",
            "<Password>testpass</Password>", "<Password>test pass</Password>"})
    public void validatePasswordInsideTextIsMasked(String password) {
        final String textTemplate = "This is a %s that will be masked";

        StringBuilder buffer = new StringBuilder(String.format(textTemplate, password));
        masker.mask(buffer);
        Assertions.assertFalse(buffer.toString().contains(password));
        Assertions.assertTrue(buffer.toString().contains("******"));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "password:testpass", "password: testpass", "password=testpass",
            "pass:testpass", "pass: testpass", "pass=testpass",
            "pwd:testpass", "pwd: testpass", "pwd=testpass",
            "<password>testpass</password>", "<password>test pass</password>",
    })
    public void validatePasswordAloneIsMasked(String password) {
        final String textTemplate = "%s";

        StringBuilder buffer = new StringBuilder(String.format(textTemplate, password));
        masker.mask(buffer);
        Assertions.assertFalse(buffer.toString().contains(password));
        Assertions.assertTrue(buffer.toString().contains("******"));
    }

    @Test
    public void validateMultiplePasswordsAreMasked() {
        final String textTemplate = "This is a %s that will be masked\nThis is a %s that will be masked";
        final String password1 = "password:testpass";
        final String password2 = "pass=testpass";
        StringBuilder buffer = new StringBuilder(String.format(textTemplate, password1, password2));
        masker.mask(buffer);

        Assertions.assertFalse(buffer.toString().contains(password1));
        Assertions.assertFalse(buffer.toString().contains(password2));
        Assertions.assertTrue(buffer.toString().contains("******"));
    }

    @Test
    public void validatePasswordAtEndOfLineMultiline() {
        final String text = "This is a password: testpass\nThis is a password: testpass";
        final String textMasked = "This is a password: ******\nThis is a password: ******";

        StringBuilder buffer = new StringBuilder(text);
        masker.mask(buffer);
        Assertions.assertEquals(textMasked, buffer.toString());
    }

    @Test
    public void testNoEndOfLine() {
        String input = "Another password:testpass2. That will be masked";
        String textMasked = "Another password:****** That will be masked";

        StringBuilder buffer = new StringBuilder(input);
        masker.mask(buffer);
        Assertions.assertEquals(textMasked, buffer.toString());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "this text contains a word with p: pet is needed",
            "<Password>test</123>", // non-valid XML
            "<Password>t" // non-valid XML
    })
    public void validateNonPasswordAreNotMasked(String password) {
        final String textTemplate = "This is a %s that will be masked";
        final String unmasked = String.format(textTemplate, password);
        StringBuilder buffer = new StringBuilder(unmasked);

        masker.mask(buffer);
        Assertions.assertEquals(unmasked, buffer.toString());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "'{\n   \"password\":\"1234\",\n   \"email\":\"testemail@gmail.com\"\n}'|'{\n   \"password\":\"******\",\n   \"email\":\"testemail@gmail.com\"\n}'",
            "'{\n   \"password\":\"test pass\",\n   \"email\":\"testemail@gmail.com\"\n}'|'{\n   \"password\":\"******\",\n   \"email\":\"testemail@gmail.com\"\n}'",
            "'{\n   \"password\":\"test\\\"pass\",\n   \"email\":\"testemail@gmail.com\"\n}'|'{\n   \"password\":\"******\",\n   \"email\":\"testemail@gmail.com\"\n}'"
    }, delimiter = '|')
    public void testJson(String unmasked, String expected) {
        final StringBuilder buffer = new StringBuilder(unmasked);

        masker.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }
}
