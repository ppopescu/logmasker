package tech.petrepopescu.logging.masker;

import tech.petrepopescu.logging.masker.abstracts.AbstractCardNumberMaskerTest;

public class CardNumberMaskerTest extends AbstractCardNumberMaskerTest {

    @Override
    protected LogMasker getLogMasker(String params) {
        CardNumberMasker masker = new CardNumberMasker();
        masker.initialize(params);
        return masker;
    }
}
