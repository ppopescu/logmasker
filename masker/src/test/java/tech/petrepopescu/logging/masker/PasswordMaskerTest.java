package tech.petrepopescu.logging.masker;

import tech.petrepopescu.logging.masker.abstracts.AbstractPasswordMaskerTest;

public class PasswordMaskerTest extends AbstractPasswordMaskerTest {
    @Override
    protected LogMasker getLogMasker(String params) {
        LogMasker passwordMasker = new PasswordMasker();
        passwordMasker.initialize(params);
        return passwordMasker;
    }
}
