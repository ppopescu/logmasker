package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import tech.petrepopescu.logging.masker.LogMasker;

import java.util.Arrays;

public abstract class AbstractMaskingTest {
    public LogMasker getLogMasker() {
        return getLogMasker(null);
    }

    protected abstract LogMasker getLogMasker(String params);

    protected MaskingConverter initializeConverter() {
        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.setMaskers(Arrays.asList(getLogMasker()));
        return maskingConverter;
    }
}
