package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public abstract class AbstractIpMaskerTest extends AbstractMaskingTest {
    protected MaskingConverter masker = initializeConverter();

    @ParameterizedTest
    @CsvSource(value = {
            "this is another IP 84.232.150.27 and nothing more|this is another IP 8*.2**.1**.2* and nothing more",
            "this is another IP 84.232.150.27. and nothing more|this is another IP 8*.2**.1**.2*. and nothing more",
            "84.232.150.27|8*.2**.1**.2*", // IP alone
            "ip=84.232.150.27|ip=8*.2**.1**.2*", // IP
            "84.232.150.27.|8*.2**.1**.2*.", // IP alone at end of sentence
            "<ip>84.232.150.27</ip>|<ip>8*.2**.1**.2*</ip>", // IP alone XML
            "1.1.1.1|1.1.1.1", // one-character IP
            "255.255.255.255|2**.2**.2**.2**", // three-characters all parts
            // multi-line
            "\'this is another IP 84.232.150.27 and nothing more\nthis is another IP 84.232.150.27 and nothing more\'|\'this is another IP 8*.2**.1**.2* and nothing more\nthis is another IP 8*.2**.1**.2* and nothing more\'",
    }, delimiter = '|')
    public void standardMaskingTest(String input, String expected) {
        final StringBuilder buffer = new StringBuilder(input);
        masker.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @Test
    public void testJson() {
        final String unmasked = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"IP\":\"255.255.255.255\"\n" +
                "}";
        final String expected = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"IP\":\"2**.2**.2**.2**\"\n" +
                "}";

        final StringBuilder buffer = new StringBuilder(unmasked);

        masker.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }
}
