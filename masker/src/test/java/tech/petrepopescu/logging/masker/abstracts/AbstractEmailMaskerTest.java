package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public abstract class AbstractEmailMaskerTest extends AbstractMaskingTest  {
    protected MaskingConverter maskingConverter = initializeConverter();

    @ParameterizedTest
    @CsvSource(value = {
            "This will mask the email test.email@domain.com and it should be masked|This will mask the email t********l@d****n.com and it should be masked",
            "This will mask the email test.email@domain.com|This will mask the email t********l@d****n.com", // end of line
            "test.email@domain.com|t********l@d****n.com", // email alone
            "test.email@domain.domain|t********l@d****n.domain", // strange domain
            "test.email@domain.co.uk|t********l@d*******o.uk", // subdomain
            "email=test.email@domain.co.uk|email=t********l@d*******o.uk", // field name
            // multi-line
            "\'This will mask the email test.email@domain.com\nThis will mask the email test.email@domain.com\'|\'This will mask the email t********l@d****n.com\nThis will mask the email t********l@d****n.com\'",
            // JSON
            "\'{\"password\":\"1234\",\"email\":\"testemail@gmail.com\"}\'|\'{\"password\":\"1234\",\"email\":\"t*******l@g***l.com\"}\'",
            "<email>testemail@gmail.com</email>|<email>t*******l@g***l.com</email>"

    }, delimiter = '|')
    public void standardMaskingTest(String unmasked, String expected) {
        final StringBuilder buffer = new StringBuilder(unmasked);
        maskingConverter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "This is not an email test@domain@test.com and it should not be masked",
            "This is not an email test@domain and it should not be masked",
            "This is not an email test@domain. and it should not be masked.",
            "This is not an email test@domain.",
            "This is not an email test@domain and it should not be masked.",
            " @ . "
    })
    public void testValuesNotMasked(String message) {
        final StringBuilder buffer = new StringBuilder(message);
        maskingConverter.mask(buffer);
        Assertions.assertEquals(message, buffer.toString());
    }

    @Test
    public void testJsonFormatted() {
        final String unmasked = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"email\":\"testemail@gmail.com\"\n" +
                "}";
        final String expected = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"email\":\"t*******l@g***l.com\"\n" +
                "}";

        final StringBuilder buffer = new StringBuilder(unmasked);

        maskingConverter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }
}
