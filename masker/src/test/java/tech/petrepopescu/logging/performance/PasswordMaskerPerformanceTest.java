package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.masker.LogMasker;
import tech.petrepopescu.logging.masker.PasswordMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class PasswordMaskerPerformanceTest extends AbstractPerformanceTest {
    @Test
    void passwordPerformanceTest() {
        List<LogMasker> maskers = Arrays.asList(new PasswordMasker());
        double min = maskerPerformanceComparisonTests(maskers);

        Assertions.assertTrue(min < 1000d);
    }
}
