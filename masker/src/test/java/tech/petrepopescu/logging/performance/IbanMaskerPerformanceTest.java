package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.masker.IbanMasker;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class IbanMaskerPerformanceTest extends AbstractPerformanceTest {
    @Test
    void ipPerformanceTest() {
        List<LogMasker> ibanMaskers = Arrays.asList(new IbanMasker());
        double min = maskerPerformanceComparisonTests(ibanMaskers);

        Assertions.assertTrue(min < 1000d);
    }
}
