package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.masker.IPMasker;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class IPMaskerPerformanceTest extends AbstractPerformanceTest {
    @Test
    void ipPerformanceTest() {
        List<LogMasker> ipMaskers = Arrays.asList(new IPMasker());
        double min = maskerPerformanceComparisonTests(ipMaskers);

        Assertions.assertTrue(min < 1000d);
    }
}
