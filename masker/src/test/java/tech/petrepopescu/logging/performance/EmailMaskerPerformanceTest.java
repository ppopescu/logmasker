package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.masker.EmailMasker;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class EmailMaskerPerformanceTest extends AbstractPerformanceTest {
    @Test
    void testPerformanceEmailMasker() {
        List<LogMasker> maskers = Arrays.asList(new EmailMasker());
        double min = maskerPerformanceComparisonTests(maskers);

        Assertions.assertTrue(min < 1000d);
    }
}
