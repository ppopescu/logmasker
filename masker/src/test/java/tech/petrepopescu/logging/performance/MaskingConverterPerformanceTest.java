package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.MaskingConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MaskingConverterPerformanceTest extends AbstractPerformanceTest {
    @Test
    void passwordPerformanceTest() {
        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.init(null);
        double min = converterPerformanceTest(maskingConverter);
        Assertions.assertTrue(min < 1000d);
    }
}
