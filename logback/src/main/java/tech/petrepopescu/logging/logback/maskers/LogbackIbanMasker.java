package tech.petrepopescu.logging.logback.maskers;

import tech.petrepopescu.logging.masker.IbanMasker;

import java.util.ArrayList;
import java.util.List;

public class LogbackIbanMasker extends IbanMasker implements LogbackLogMasker {
    private final List<String> countryCodes = new ArrayList<>();

    public void addCountryCode(String countryCode) {
        this.countryCodes.add(countryCode);
        this.populateCountriesToCheck(countryCodes.toArray(new String[0]));
    }
}
