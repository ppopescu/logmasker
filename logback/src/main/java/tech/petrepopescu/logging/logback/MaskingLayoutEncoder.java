package tech.petrepopescu.logging.logback;

import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import tech.petrepopescu.logging.MaskingConverter;
import tech.petrepopescu.logging.masker.LogMasker;
import tech.petrepopescu.logging.logback.maskers.LogbackLogMasker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MaskingLayoutEncoder extends PatternLayoutEncoder {
    private final MaskingConverter maskingConverter;

    private final List<LogbackLogMasker> maskers = new ArrayList<>();

    private char maskChar = '*';

    public MaskingLayoutEncoder() {
        this.maskingConverter = new MaskingConverter();
        maskingConverter.init(null);
    }

    public void addMasker(LogbackLogMasker newMasker) {
        this.maskers.add(newMasker);

        maskingConverter.setMaskers(maskers.stream().map(masker -> {
            LogMasker logMasker = masker.toLogMasker();
            logMasker.setMaskChar(this.maskChar);
            return logMasker;
        }).collect(Collectors.toList()));
    }

    public void setMaskChar(String maskChar) {
        if (maskChar != null && maskChar.length() == 1) {
            this.maskChar = maskChar.charAt(0);
            maskingConverter.setMaskers(maskers.stream().map(masker -> {
                LogMasker logMasker = masker.toLogMasker();
                logMasker.setMaskChar(this.maskChar);
                return logMasker;
            }).collect(Collectors.toList()));
        }
    }

    @Override
    public byte[] encode(ILoggingEvent event) {
        StringBuilder masked = new StringBuilder(event.getMessage());
        maskingConverter.mask(masked);

        MaskableLoggingEvent updatedEvent = new MaskableLoggingEvent(event);
        updatedEvent.setMessage(masked.toString());
        return super.encode(updatedEvent);
    }
}
