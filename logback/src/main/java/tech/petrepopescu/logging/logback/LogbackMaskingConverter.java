package tech.petrepopescu.logging.logback;

import ch.qos.logback.classic.pattern.MessageConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import tech.petrepopescu.logging.MaskingConverter;

public class LogbackMaskingConverter extends MessageConverter {
    private MaskingConverter maskingConverter;

    @Override
    public void start() {
        maskingConverter = new MaskingConverter();
        try {
            maskingConverter.init(getOptionList());
        } catch (Exception e) {
            System.err.println(e);
        }
        super.start();
    }

    @Override
    public String convert(ILoggingEvent event) {
        StringBuilder messageBuffer = new StringBuilder(event.getFormattedMessage());
        maskingConverter.mask(messageBuffer);
        return messageBuffer.toString();
    }
}
