package tech.petrepopescu.logging.log4j2.maskers;

import tech.petrepopescu.logging.masker.LogMasker;

public interface Log4jLogMasker extends LogMasker {
    default LogMasker toLogMasker() {
        return this;
    }
}
