package tech.petrepopescu.logging.log4j2;

import tech.petrepopescu.logging.log4j2.utils.ListAppender;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class MultiThreadedTest {
    private static ListAppender listAppender;
    private static final Logger logger = LogManager.getLogger(MultiThreadedTest.class);

    private static final int NO_THREADS = 10;
    private static final int LOGS_PER_THREAD = 100;
    private static final List<String> TEST_LOGGED_LINES = Arrays.asList("This will mask the email test.email@domain.com",
            "This will mask the email testemail@domain.com",
            "This is a password: testpass",
            "Another password:testpass2. That will be masked",
            "Nothing will be masked here",
            "this is another IP 84.232.150.27 and nothing more",
            "this is another IP 1.1.1.1 and nothing more",
            "this is a card 4916246076443617 and it should be masked",
            "this is a card 377235414017308 and it should be masked",
            "this is a iban IE64IRCE92050112345678 and it should be masked",
            "this is a iban GT20AGRO00000000001234567890 and it should be masked");

    private static final List<String> MUST_NOT_CONTAIN = Arrays.asList("test.email@domain.com", "testemail@domain.com", "testpass", "testpass2",
            "84.232.150.27", "4916246076443617", "377235414017308", "IE64IRCE92050112345678", "GT20AGRO00000000001234567890");
    private static final List<String> MUST_CONTAIN = Arrays.asList("t********l@d****n.com", "t*******l@d****n.com", "password: ******",
            "8*.2**.1**.2*", "1.1.1.1", "4*********443617", "3********017308", "IE6***************5678", "GT2*********************7890");

    @BeforeAll
    public static void prepare() throws Exception {
        listAppender = new ListAppender();
        listAppender.start();
        org.apache.logging.log4j.core.Logger coreLogger = (org.apache.logging.log4j.core.Logger)logger;
        coreLogger.addAppender(listAppender);
    }

    @AfterAll
    public static void cleanup() {
        listAppender.clear();
        listAppender.stop();
    }

    @BeforeEach
    public void clear() {
        listAppender.clear();
    }

    @Test
    void testAppender() {
        logger.info("This is a test message");
        Assertions.assertEquals(1, listAppender.getLoggedMessages().size());
    }

    @Test
    void testMultiThreaded() throws Exception {
        Set<Integer> loggedLines = Collections.synchronizedSet(new HashSet<>());
        ExecutorService es = Executors.newCachedThreadPool();
        for(int count=0; count<NO_THREADS; count++)
            es.execute(() -> {
                Random random = new Random();
                for (int logCount = 0; logCount < LOGS_PER_THREAD; logCount++) {
                    int line = random.nextInt(TEST_LOGGED_LINES.size());
                    loggedLines.add(line);
                    logger.info(TEST_LOGGED_LINES.get(line));
                }
            });
        es.shutdown();
        es.awaitTermination(1, TimeUnit.MINUTES);

        List<String> loggedMessages = listAppender.getLoggedMessages();
        Assertions.assertEquals(NO_THREADS * LOGS_PER_THREAD, loggedMessages.size());
        Assertions.assertEquals(TEST_LOGGED_LINES.size(), loggedLines.size());

        String fullLogText = loggedMessages.stream().collect(Collectors.joining("\n"));
        for (String mustNotContainLine: MUST_NOT_CONTAIN) {
            Assertions.assertFalse(StringUtils.contains(fullLogText, mustNotContainLine), "Line exist: " + mustNotContainLine);
        }

        for (String mustContainLine: MUST_CONTAIN) {
            Assertions.assertTrue(StringUtils.contains(fullLogText, mustContainLine), "Line does not exist: " + mustContainLine);
        }
    }
}
