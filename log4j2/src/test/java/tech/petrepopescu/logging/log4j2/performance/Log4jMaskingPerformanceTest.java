package tech.petrepopescu.logging.log4j2.performance;

import tech.petrepopescu.logging.log4j2.utils.AbstractPerformanceTest;
import tech.petrepopescu.logging.log4j2.Log4jMaskingConverter;
import org.junit.jupiter.api.Test;

class Log4jMaskingPerformanceTest extends AbstractPerformanceTest {

    @Test
    void performanceTest() {
        final Log4jMaskingConverter maskingConverter = Log4jMaskingConverter.newInstance(null);
        performanceTest(LOG_TEST_LINES, maskingConverter, "ALL");
    }
}
