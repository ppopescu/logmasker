package tech.petrepopescu.logging.log4j2.utils;

import tech.petrepopescu.logging.log4j2.Log4jMaskingConverter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListAppender extends AbstractAppender {
    private final List<String> messages = Collections.synchronizedList(new ArrayList<>());
    private final Log4jMaskingConverter log4jMaskingConverter;

    public ListAppender() throws Exception {
        super("ListAppender", null, null, false, null);
        log4jMaskingConverter = Log4jMaskingConverter.newInstance(null);
    }

    @Override
    public void append(LogEvent event) {
        StringBuilder builder = new StringBuilder();
        log4jMaskingConverter.format(event, builder);
        messages.add(builder.toString());
    }

    public void clear() {
        this.messages.clear();
    }

    public List<String> getLoggedMessages() {
        return this.messages;
    }
}
