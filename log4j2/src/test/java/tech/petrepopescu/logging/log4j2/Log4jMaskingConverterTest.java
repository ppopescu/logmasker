package tech.petrepopescu.logging.log4j2;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFormatMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tech.petrepopescu.logging.log4j2.Log4jMaskingConverter;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

class Log4jMaskingConverterTest {
    protected static final List<String> TEST_LOGGED_LINES = Arrays.asList("This will mask the email test.email@domain.com",
            "This will mask the email testemail@domain.com",
            "This is a password: testpass",
            "Another password:testpass2. That will be masked",
            "Nothing will be masked here",
            "this is another IP 84.232.150.27 and nothing more",
            "this is another IP 1.1.1.1 and nothing more",
            "this is a card 4916246076443617 and it should be masked",
            "this is a card 377235414017308 and it should be masked",
            "this is a iban IE64IRCE92050112345678 and it should be masked",
            "this is a iban GT20AGRO00000000001234567890 and it should be masked");

    private static final int NO_LINES = 1_000;

    @Test
    void testMasker() {
        Log4jMaskingConverter log4jMaskingConverter = Log4jMaskingConverter.newInstance(null);
        StringBuilder unmasked = new StringBuilder();
        StringBuilder masked = new StringBuilder();
        Random random = new Random();
        for (int count = 0; count<NO_LINES; count++) {
            int lineNo = random.nextInt(TEST_LOGGED_LINES.size());
            unmasked.append(TEST_LOGGED_LINES.get(lineNo) + "\n");

            // We have tests that validate that single-line masking works, so use it to build the expected output
            StringBuilder builder = new StringBuilder();
            Message message = new MessageFormatMessage(TEST_LOGGED_LINES.get(lineNo));
            LogEvent logEvent = Log4jLogEvent.newBuilder().setMessage(message).build();
            log4jMaskingConverter.format(logEvent, builder);
            masked.append(builder + "\n");
        }

        long nanoStart = System.nanoTime();
        StringBuilder builder = new StringBuilder();
        Message message = new MessageFormatMessage(unmasked.toString());
        LogEvent logEvent = Log4jLogEvent.newBuilder().setMessage(message).build();
        log4jMaskingConverter.format(logEvent, builder);
        long nanoEnd = System.nanoTime();
        long time = (nanoEnd-nanoStart)/1_000_000;

        System.out.println("Duration: " + time + "ms");

        Assertions.assertEquals(masked.toString().trim(), builder.toString().trim());
    }
}
