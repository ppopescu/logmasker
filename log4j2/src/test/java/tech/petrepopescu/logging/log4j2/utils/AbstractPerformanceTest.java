package tech.petrepopescu.logging.log4j2.utils;

import tech.petrepopescu.logging.log4j2.Log4jMaskingConverter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFormatMessage;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Performance test suite that can be used to check execution times and compare two or more maskers.
 * Useful if you are writing a new version of a masker and want to check how it performs compared to the existing one
 */
public abstract class AbstractPerformanceTest {
    protected static final List<String> LOG_TEST_LINES = Arrays.asList("This will mask the email test.email@domain.com",
            "This will mask the email testemail@domain.com",
            "This is a password: testpass",
            "Another password:testpass2. That will be masked",
            "Nothing will be masked here",
            "this is another IP 84.232.150.27 and nothing more",
            "this is another IP 1.1.1.1 and nothing more",
            "this is a card 4916246076443617 and it should be masked",
            "this is a card 377235414017308 and it should be masked",
            "this is a iban IE64IRCE92050112345678 and it should be masked",
            "this is a iban GT20AGRO00000000001234567890 and it should be masked");

    private static final int TOTAL_RUNS = 10;
    private static final int NUMBER_OF_LINES = 100_000;
    private static final boolean LOG_INDIVIDUAL_RUNS = false;

    public double performanceTest(List<String> lines, Log4jMaskingConverter maskingConverter, String testName) {
        Consumer<String> buildAndMaskEvent = (String line) -> {
            StringBuilder builder = new StringBuilder();
            Message message = new MessageFormatMessage(line);
            LogEvent logEvent = Log4jLogEvent.newBuilder().setMessage(message).build();
            maskingConverter.format(logEvent, builder);
        };

        return performanceTest(lines, buildAndMaskEvent, testName);
    }

    protected double performanceTest(List<String> lines, Consumer<String> maskerCallerFunction, String testName) {
        System.out.println("Starting performance test for " + testName + "\n\tTotal runs: " + TOTAL_RUNS + "\n\tNumber of lines per run: " + NUMBER_OF_LINES);
        final List<Long> executionTimes = new ArrayList<>();
        for (int run = 0; run < TOTAL_RUNS; run++) {
            List<String> toMask = new ArrayList<>();
            Random random = new Random();
            for (int count = 0; count < NUMBER_OF_LINES; count++) {
                toMask.add(lines.get(random.nextInt(lines.size())));
            }

            long start = System.nanoTime();
            for (String line:toMask) {
                maskerCallerFunction.accept(line);
            }

            long end = System.nanoTime();
            long time = (end-start)/1_000_000;
            executionTimes.add(time);
            if (LOG_INDIVIDUAL_RUNS) {
                System.out.println("Duration for run " + run + " was: " + time + "ms");
            }
        }

        long totalTime = executionTimes.stream().collect(Collectors.summingLong(Long::longValue));
        double averageTime = ((double)totalTime)/((double) TOTAL_RUNS);
        System.out.println("Average duration was: " + averageTime + "ms\n");

        return averageTime;
    }
}
